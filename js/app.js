let employeeList = JSON.parse(localStorage.getItem("EMPLOYEE_DATA")) || [];

function init() {
  employeeList = employeeList.map((employee) => {
    return createNewEmployee(employee);
  });
  renderToTable(employeeList);
}

function addEmployee(newEmployee) {
  employeeList.push(newEmployee);
}

function deleteEmployee(employeeId) {
  employeeList = employeeList.filter((employee) => {
    return employee.id !== employeeId;
  });
}

function updateEmployee(employeeId, updatedEmployee) {
  employeeList = employeeList.map((employee) => {
    return employee.id === employeeId ? updatedEmployee : employee;
  });
}

function searchEmployee(searchValue, employeeList) {
  return employeeList.filter((employee) => {
    return employee.rating
      .toLowerCase()
      .includes(searchValue.trim().toLowerCase());
  });
}

document.querySelector("#btnThem").addEventListener("click", function () {
  resetFormInput();
  removeFormStyle();
  lockElement({ btnCapNhat });
  unlockElement({ btnThemNV, tknv });
});

document.querySelector("#btnThemNV").addEventListener("click", function () {
  const newEmployee = createNewEmployee(takeFormValue());
  if (!isValidate(newEmployee, employeeList)) {
    return;
  }
  addEmployee(newEmployee);
  renderToTable(employeeList);
  hideModal();
  saveToLocalStorage(employeeList);
});

document
  .querySelector("#tableDanhSach")
  .addEventListener("click", function (event) {
    if (
      event.target.tagName === "BUTTON" &&
      event.target.textContent === "Xóa"
    ) {
      deleteEmployee(event.target.id);
      renderToTable(employeeList);
      saveToLocalStorage(employeeList);
    }
  });

document
  .querySelector("#tableDanhSach")
  .addEventListener("click", function (event) {
    if (
      event.target.tagName === "BUTTON" &&
      event.target.textContent === "Sửa"
    ) {
      let updateEmployee = findEmployeeById(event.target.id, employeeList);
      showEmployeeValueOnForm(updateEmployee);
      removeFormStyle();
      lockElement({ btnThemNV, tknv });
      unlockElement({ btnCapNhat });
    }
  });

document.querySelector("#btnCapNhat").addEventListener("click", function () {
  let updatedEmployee = new createNewEmployee(takeFormValue());
  if (!isValidate(updatedEmployee)) {
    return;
  }
  updateEmployee(updatedEmployee.id, updatedEmployee);
  renderToTable(employeeList);
  hideModal();
  saveToLocalStorage(employeeList);
});

document.querySelector("#btnTimNV").addEventListener("click", function () {
  const searchValue = document.querySelector("#searchName").value;
  const filterEmployeeList = searchEmployee(searchValue, employeeList);
  renderToTable(filterEmployeeList);
  resetInput("#searchName");
});

document.querySelector("#btnUndo").addEventListener("click", function () {
  renderToTable(employeeList);
});

init();
