class Employee {
  constructor(employeeObj) {
    this.id = employeeObj.id;
    this.name = employeeObj.name;
    this.email = employeeObj.email;
    this.password = employeeObj.password;
    this.workingDate = employeeObj.workingDate;
    this.basicSalary = employeeObj.basicSalary;
    this.position = employeeObj.position;
    this.workingHour = employeeObj.workingHour;
  }

  get totalSalary() {
    if (this.position === "Giám đốc") {
      return new Intl.NumberFormat("vn-VN").format(+this.basicSalary * 3);
    } else if (this.position === "Trưởng phòng") {
      return new Intl.NumberFormat("vn-VN").format(+this.basicSalary * 2);
    } else {
      return new Intl.NumberFormat("vn-VN").format(+this.basicSalary);
    }
  }

  get rating() {
    if (+this.workingHour > 192) {
      return "Xuất sắc";
    } else if (+this.workingHour > 176) {
      return "Giỏi";
    } else if (+this.workingHour > 160) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  }
}
