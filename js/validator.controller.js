const idInput = document.querySelector("#tknv");
const nameInput = document.querySelector("#name");
const emailInput = document.querySelector("#email");
const passwordInput = document.querySelector("#password");
const workingDateInput = document.querySelector("#datepicker");
const basicSalaryInput = document.querySelector("#luongCB");
const positionInput = document.querySelector("#chucvu");
const workingHourInput = document.querySelector("#gioLam");

function isFill(value) {
  return value === "" ? false : true;
}

function isDuplicateId(id, dataList) {
  if (!dataList) {
    return false;
  }

  return dataList.findIndex((item) => item.id === id) !== -1;
}

function isBetween(value, min, max) {
  return +value < min || +value > max ? false : true;
}

function isText(value) {
  return Number.isNaN(+value) ? true : false;
}

function isNumber(value) {
  return Number.isNaN(+value) ? false : true;
}

function isValidPassword(password) {
  const regex =
    /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,10}$/;
  return regex.test(password);
}

function isValidEmail(email) {
  const regex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  return regex.test(email);
}

function isValidDate(date) {
  let regex =
    /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;

  return regex.test(date);
}

function isDateBeforeToday(date) {
  let inputDate = new Date(date).setHours(0, 0, 0, 0);
  let today = new Date().setHours(0, 0, 0, 0);

  return inputDate > today ? false : true;
}

function showError(input, message) {
  let formGroup = input.parentElement.parentElement;
  let errorDisplay = formGroup.querySelector(".sp-thongbao");
  input.style.border = "3px solid red";
  errorDisplay.textContent = message;
  errorDisplay.style.display = "block";
}

function showSuccess(input) {
  let formGroup = input.parentElement.parentElement;
  let errorDisplay = formGroup.querySelector(".sp-thongbao");
  input.style.border = "3px solid green";
  errorDisplay.style.display = "none";
}

function removeFormStyle() {
  let formGroups = document.querySelectorAll(".form-group");
  formGroups.forEach((formGroup) => {
    if (formGroup.querySelector("input")) {
      formGroup.querySelector("input").style.removeProperty("border");
    } else {
      formGroup.querySelector("select").style.removeProperty("border");
    }
    formGroup.querySelector(".sp-thongbao").style.display = "none";
  });
}

function checkId(id, dataList) {
  let valid = false;

  if (isDuplicateId(id, dataList)) {
    showError(idInput, "Tài khoản nhân viên không được trùng");
  } else if (!isFill(id)) {
    showError(idInput, "Tài khoản nhân viên không được để trống");
  } else if (!isBetween(id.length, 4, 6)) {
    showError(idInput, "Tải khoản chỉ được bao gồm 4-6 ký tự");
  } else {
    valid = true;
    showSuccess(idInput);
  }

  return valid;
}

function checkName(name) {
  let valid = false;

  if (!isFill(name)) {
    showError(nameInput, "Tên nhân viên không được để trống");
  } else if (!isText(name)) {
    showError(nameInput, "Tên nhân viên phải là chữ");
  } else {
    valid = true;
    showSuccess(nameInput);
  }

  return valid;
}

function checkEmail(email) {
  let valid = false;

  if (!isFill(email)) {
    showError(emailInput, "Email nhân viên không được để trống");
  } else if (!isValidEmail(email)) {
    showError(emailInput, "Email nhân viên không hợp lệ");
  } else {
    valid = true;
    showSuccess(emailInput);
  }

  return valid;
}

function checkPassword(password) {
  let valid = false;

  if (!isFill(password)) {
    showError(passwordInput, "Mật khẩu không được để trống");
  } else if (!isValidPassword(password)) {
    showError(
      passwordInput,
      "Mật khẩu phải bao gồm từ 6-10 ký tự (Ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
  } else {
    valid = true;
    showSuccess(passwordInput);
  }

  return valid;
}

function checkWorkingDate(workingDate) {
  let valid = false;
  if (!isFill(workingDate)) {
    showError(workingDateInput, "Ngày không được để trống");
  } else if (!isValidDate(workingDate)) {
    showError(workingDateInput, "Ngày không đúng định dạng");
  } else if (!isDateBeforeToday(workingDate)) {
    showError(workingDateInput, "Ngày không hợp lệ");
  } else {
    valid = true;
    showSuccess(workingDateInput);
  }

  return valid;
}

function checkBasicSalary(basicSalary) {
  let valid = false;
  if (!isFill(basicSalary)) {
    showError(basicSalaryInput, "Lương cơ bản không được để trống");
  } else if (!isNumber(basicSalary)) {
    showError(basicSalaryInput, "Lương cơ bản phải là số");
  } else if (!isBetween(basicSalary, 1000000, 20000000)) {
    showError(
      basicSalaryInput,
      "Lương cơ bản chỉ nằm trong khoảng 1.000.000 đến 20.000.000 VNĐ"
    );
  } else {
    valid = true;
    showSuccess(basicSalaryInput);
  }

  return valid;
}

function checkPosition(position) {
  let valid = false;

  if (!isFill(position)) {
    showError(positionInput, "Vị trí không được để trống");
  } else {
    valid = true;
    showSuccess(positionInput);
  }

  return valid;
}

function checkWorkingHour(workingHour) {
  let valid = false;

  if (!isFill(workingHour)) {
    showError(workingHourInput, "Số giờ làm việc không được để trống");
  } else if (!isBetween(workingHour, 80, 200)) {
    showError(
      workingHourInput,
      "Số giờ làm việc chỉ nằm trong khoảng 80-200 giờ"
    );
  } else {
    valid = true;
    showSuccess(workingHourInput);
  }

  return valid;
}

function isValidate(employee, employeeList) {
  let isEmployeeIdValid = checkId(employee.id, employeeList);
  let isEmployeeNameValid = checkName(employee.name);
  let isEmployeeEmailValid = checkEmail(employee.email);
  let isEmployeePasswordValid = checkPassword(employee.password);
  let isEmployeeWorkingDateValid = checkWorkingDate(employee.workingDate);
  let isEmployeeBasicSalaryValid = checkBasicSalary(employee.basicSalary);
  let isEmployeePositionValid = checkPosition(employee.position);
  let isEmployeeWorkingHourValid = checkWorkingHour(employee.workingHour);

  return (
    isEmployeeIdValid &&
    isEmployeeNameValid &&
    isEmployeeEmailValid &&
    isEmployeePasswordValid &&
    isEmployeeWorkingDateValid &&
    isEmployeeBasicSalaryValid &&
    isEmployeePositionValid &&
    isEmployeeWorkingHourValid
  );
}
