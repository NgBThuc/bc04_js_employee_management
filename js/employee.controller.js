function takeFormValue() {
  const id = document.querySelector("#tknv").value;
  const name = document.querySelector("#name").value;
  const email = document.querySelector("#email").value;
  const password = document.querySelector("#password").value;
  const workingDate = document.querySelector("#datepicker").value;
  const basicSalary = document.querySelector("#luongCB").value;
  const position = document.querySelector("#chucvu").value;
  const workingHour = document.querySelector("#gioLam").value;

  return {
    id,
    name,
    email,
    password,
    workingDate,
    basicSalary,
    position,
    workingHour,
  };
}

function resetInput(inputSelector) {
  let input = document.querySelector(inputSelector);
  input.value = "";
}

function resetFormInput() {
  resetInput("#tknv");
  resetInput("#name");
  resetInput("#email");
  resetInput("#password");
  resetInput("#datepicker");
  resetInput("#luongCB");
  resetInput("#chucvu");
  resetInput("#gioLam");
}

function createNewEmployee(employeeInfo) {
  return new Employee(employeeInfo);
}

function renderToTable(employeeList) {
  let tableMarkup = "";

  employeeList.forEach((employee) => {
    tableMarkup += `
      <tr>
        <td>${employee.id}</td>
        <td>${employee.name}</td>
        <td>${employee.email}</td>
        <td>${employee.workingDate}</td>
        <td>${employee.position}</td>
        <td>${employee.totalSalary}</td>
        <td>${employee.rating}</td>
        <td>
          <button id="${employee.id}" class="btn btn-danger">Xóa</button>        
          <button 
            id="${employee.id}"
            class="btn btn-warning"
            data-toggle="modal"
            data-target="#myModal"
          >Sửa</button>      
        </td>
      </tr>
    `;
  });

  document.querySelector("#tableDanhSach").innerHTML = tableMarkup;
}

function showEmployeeValueOnForm(employee) {
  document.querySelector("#tknv").value = employee.id;
  document.querySelector("#name").value = employee.name;
  document.querySelector("#email").value = employee.email;
  document.querySelector("#password").value = employee.password;
  document.querySelector("#datepicker").value = employee.workingDate;
  document.querySelector("#luongCB").value = employee.basicSalary;
  document.querySelector("#chucvu").value = employee.position;
  document.querySelector("#gioLam").value = employee.workingHour;
}

function saveToLocalStorage(employeeList) {
  localStorage.setItem("EMPLOYEE_DATA", JSON.stringify(employeeList));
}

function findEmployeeById(employeeId, employeeList) {
  return employeeList.find((employee) => employee.id === employeeId);
}

function hideModal() {
  $("#myModal").modal("hide");
}

function lockElement(buttonObj) {
  for (let button in buttonObj) {
    document.getElementById(button).disabled = true;
  }
}

function unlockElement(buttonObj) {
  for (let button in buttonObj) {
    document.getElementById(button).disabled = false;
  }
}
